from django.conf.urls import url
from django.views.generic import ListView, DetailView
from models import Inv
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^signup', views.signup, name='signup'),
    url(r'^signin', views.signin, name='signin'),
    url(r'^profile', views.profile, name='profile'),
    url(r'^inventory', ListView.as_view(queryset=Inv.objects.all().order_by("type"),template_name="inventory/inventory.html")),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(model = Inv, template_name = "inventory/display.html"))
    ]