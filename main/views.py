# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template import Context

#Handle post
from django.template import RequestContext

from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

from forms import SignUpForm


# Create your views here.

# context = { 'loop_times': range(2018, 1920, -1) }
def home(request):
    return render(request, 'main/home.html', {})


def signup(request):
    if request.method == 'POST':
        print 'Validating form...'
        form = SignUpForm(request.POST)
        if form.is_valid():
            print 'Form is valid'
            form.save()
            print 'Saved form'

            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            print username
            user = form.save()#(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
        else:
            print form.errors
            print form.error_messages
            print 'Form is not valid'
    else:
        form = SignUpForm()
    return render(request, 'signup/signup.html', {'form': form}, RequestContext(request))

def signin(request):
    return render(request, 'signin/signin.html', {})

def profile(request):
    return render(request, 'profile/profile.html', {})
    
def inventory(request):
    return render(request, 'inventory/inventory.html', {})
