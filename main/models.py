# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Inv(models.Model):
    type = models.CharField(max_length=50)
    condition = models.TextField(max_length=20)
    
    def __unicode__(self):
        return self.type