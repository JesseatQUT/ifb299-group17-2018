-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ms-django-db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ms-django-db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ms-django-db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` ;
USE `ms-django-db` ;

-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`django_content_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`django_content_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app_label` VARCHAR(100) NOT NULL,
  `model` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label` ASC, `model` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_permission` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `content_type_id` INT(11) NOT NULL,
  `codename` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id` ASC, `codename` ASC),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `ms-django-db`.`django_content_type` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 22
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_group_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_group_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id` ASC, `permission_id` ASC),
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id` ASC),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`
    FOREIGN KEY (`permission_id`)
    REFERENCES `ms-django-db`.`auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `ms-django-db`.`auth_group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(128) NOT NULL,
  `last_login` DATETIME NULL DEFAULT NULL,
  `is_superuser` TINYINT(1) NOT NULL,
  `username` VARCHAR(150) NOT NULL,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(30) NOT NULL,
  `email` VARCHAR(254) NOT NULL,
  `is_staff` TINYINT(1) NOT NULL,
  `is_active` TINYINT(1) NOT NULL,
  `date_joined` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_user_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_user_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id` ASC, `group_id` ASC),
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id` ASC),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `ms-django-db`.`auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `ms-django-db`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`auth_user_user_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`auth_user_user_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id` ASC, `permission_id` ASC),
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id` ASC),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`
    FOREIGN KEY (`permission_id`)
    REFERENCES `ms-django-db`.`auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `ms-django-db`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`contact` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `contact_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`django_admin_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`django_admin_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `action_time` DATETIME NOT NULL,
  `object_id` LONGTEXT NULL DEFAULT NULL,
  `object_repr` VARCHAR(200) NOT NULL,
  `action_flag` SMALLINT(5) UNSIGNED NOT NULL,
  `change_message` LONGTEXT NOT NULL,
  `content_type_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id` ASC),
  INDEX `django_admin_log_user_id_c564eba6_fk` (`user_id` ASC),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `ms-django-db`.`django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `ms-django-db`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`django_migrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`django_migrations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `applied` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`django_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`django_session` (
  `session_key` VARCHAR(40) NOT NULL,
  `session_data` LONGTEXT NOT NULL,
  `expire_date` DATETIME NOT NULL,
  PRIMARY KEY (`session_key`),
  INDEX `django_session_expire_date_a5c62663` (`expire_date` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`instrument`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`instrument` (
  `instrument_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `cost` DECIMAL(10,0) NOT NULL,
  PRIMARY KEY (`instrument_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`instruments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`instruments` (
  `instrumentID` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`instrumentID`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`inventory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`inventory` (
  `instrument_serial` INT(11) NOT NULL,
  `is_available` INT(11) NULL DEFAULT NULL,
  `type_ID` INT(11) NOT NULL,
  PRIMARY KEY (`instrument_serial`),
  UNIQUE INDEX `instrument_serial_UNIQUE` (`instrument_serial` ASC),
  INDEX `fk_inventory_instrument_idx` (`type_ID` ASC),
  CONSTRAINT `fk_inventory_instrument`
    FOREIGN KEY (`type_ID`)
    REFERENCES `mydb`.`instrument` (`instrument_id`),
  CONSTRAINT `typ`
    FOREIGN KEY (`type_ID`)
    REFERENCES `ms-django-db`.`instruments` (`instrumentID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`teacher` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `DOB` DATE NOT NULL,
  `gender` ENUM('MALE', 'FEMALE', 'OTHER') NOT NULL,
  `phone_ID` INT(11) NOT NULL,
  `alma_mater` VARCHAR(45) NULL DEFAULT NULL,
  `fb_profile` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `fk_Teacher_Contact1`
    FOREIGN KEY (`ID`)
    REFERENCES `ms-django-db`.`contact` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`student` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `DOB` DATE NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `gender` ENUM('MALE', 'FEMALE', 'OTHER') NOT NULL,
  `phone_ID` INT(11) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `fb_profile` VARCHAR(45) NULL DEFAULT NULL,
  `registration_date` DATE NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `fk_Student_Contact1`
    FOREIGN KEY (`ID`)
    REFERENCES `ms-django-db`.`contact` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`lesson_contract`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`lesson_contract` (
  `Contract_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Instrument_ID` INT(11) NOT NULL,
  `Student_ID` INT(11) NOT NULL,
  `Teacher_ID` INT(11) NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `duration` ENUM('30', '60') NOT NULL,
  PRIMARY KEY (`Contract_ID`, `Instrument_ID`, `Student_ID`, `Teacher_ID`),
  INDEX `teacher_id_idx` (`Teacher_ID` ASC),
  INDEX `student_id` (`Student_ID` ASC),
  INDEX `instrument_id` (`Instrument_ID` ASC),
  CONSTRAINT `instrument_id`
    FOREIGN KEY (`Instrument_ID`)
    REFERENCES `ms-django-db`.`instruments` (`instrumentID`),
  CONSTRAINT `l.teacher_id`
    FOREIGN KEY (`Teacher_ID`)
    REFERENCES `ms-django-db`.`teacher` (`ID`),
  CONSTRAINT `student_id`
    FOREIGN KEY (`Student_ID`)
    REFERENCES `ms-django-db`.`student` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`lesson_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`lesson_detail` (
  `lesson_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `lesson_contract` INT(11) NOT NULL,
  `date_time` DATETIME NOT NULL,
  PRIMARY KEY (`lesson_ID`, `lesson_contract`),
  INDEX `lessonid` (`lesson_contract` ASC),
  CONSTRAINT `lesson_id`
    FOREIGN KEY (`lesson_contract`)
    REFERENCES `ms-django-db`.`lesson_contract` (`Contract_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`main_inv`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`main_inv` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(50) NOT NULL,
  `condition` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`teacher_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`teacher_detail` (
  `Teacher_ID` INT(11) NOT NULL,
  `teaches_instrument` INT(11) NOT NULL,
  `hourly_rate` DECIMAL(10,0) NOT NULL,
  `skill_level` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Teacher_ID`),
  CONSTRAINT `Teacher_id`
    FOREIGN KEY (`Teacher_ID`)
    REFERENCES `ms-django-db`.`teacher` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ms-django-db`.`teacher_language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ms-django-db`.`teacher_language` (
  `Teacher_ID` INT(11) NOT NULL,
  `LANG_CODE_ISO639-2` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Teacher_ID`),
  CONSTRAINT `fk_teacher_id`
    FOREIGN KEY (`Teacher_ID`)
    REFERENCES `ms-django-db`.`teacher` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

USE `mydb` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
